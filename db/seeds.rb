# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require_relative './seeds/helper'

puts 'Povoando os dados...'

User.destroy_all
ActivityType.destroy_all
Activity.destroy_all
Level.destroy_all

Level.create!(start_range: 0, end_range: 50, name: 'Obi-Wan Kenobi (Star Wars)', description: 'Que a Força esteja com você')
Level.create!(start_range: 51, end_range: 200, name: 'Leônidas de Esparta (40)', description: 'This is SERPROOOOO…')
Level.create!(start_range: 201, end_range: 400, name: 'Magnus Carlsen (Xadrez)', description: 'Sem o elemento de prazer, não vale a pena tentar se destacar em nada.')
Level.create!(start_range: 401, end_range: 800, name: 'Steve Jobs (TI)', description: 'Design não é apenas como se parece. Design é como funciona')
Level.create!(start_range: 801, end_range: 1600, name: 'Sherlock Holmes', description: 'Agora é xeque e, por acaso, mate')
Level.create!(start_range: 1601, end_range: 3200, name: 'Mark Zuckerberg (TI)', description: 'Quando damos a toda a gente uma voz e damos a toda a gente poder, o sistema acaba por se tornar um lugar realmente melhor')
Level.create!(start_range: 3201, end_range: 6400, name: 'Han Solo (Star Wars)', description: 'É quando a diversão começa…')
Level.create!(start_range: 6401, end_range: 12800, name: 'Linus Torvalds (TI)', description: 'Sério, eu não tenho por meta destruir a Microsoft. Este será um efeito colateral completamente involuntário')
Level.create!(start_range: 12801, end_range: 25600, name: 'Batman (Bruce Wayne)', description: 'Não é quem eu sou por dentro e sim, o que eu faço é que me define')
Level.create!(start_range: 25601, end_range: 999999, name: 'Gandhi', description: 'Temos de nos tornar na mudança que queremos ver. ')

Source.destroy_all
source = Source.create!(name: '#você.serpro')

BadgeGroup.destroy_all
Badge.destroy_all
#badge_group = BadgeGroup.create!(:name => 'Votos em Iniciativas do Planejamento Estratégico')
#badge = Badge.create!(name: 'Entrada no Game ', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','entrada_ouro.png'),'rb'))
#user.badges<< badge
#badge = Badge.create!(name: 'Entrada no Game Prata', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','entrada_prata.png'),'rb'))
#user.badges<< badge
#badge = Badge.create!(name: 'Entrada no Game Bronze', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','entrada_bronze.png'),'rb'))
#user.badges<< badge

Category.destroy_all
category_process = Category.create!(name: 'Processos Internos')


Rule.destroy_all
badge_group = BadgeGroup.create!(:name => 'Entrada na Participação do Planejamento Estratégico', :description => 'Ganha ao acessar com login e senha o sítio "Meu Serpro".')
badge = Badge.create!(name: 'Entrada na Participação', description: 'Ganha ao acessar com login e senha o sítio "Meu Serpro"', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','badge_entrada_ouro.png'),'rb'))
activity_type = ActivityType.create!(name: 'Entrar no Jogo do Planejamento', description: 'Envolvimento no início de lançamento na gamificação demonstrando engajamento', points_definition: 10, source: source, category: category_process, identifier: 'enter_game' )
Rule.create!(name: 'Inicar Participação', activity_type: activity_type, badge: badge , amount: 1)

#badge_group = BadgeGroup.create!(:name => 'Criação de Novas Iniciativas para o Planejamento Estratégico')
#activity_type = ActivityType.create!(name: 'Propor uma ideia no Planejamento', description: 'Envolvimento do funcionario enviando novas propostas', points_definition: 100, source: source, category: category_process, identifier: 'make_proposal' )

badge_group = BadgeGroup.create!(:name => 'Receber Voto em Ideia Postada para o Planejamento Estratégico')
activity_type = ActivityType.create!(name: 'Receber Voto em Ideia', description: 'Envolvimento do funcionario enviando novas propostas e tendo votos nestas propostas', points_definition: 40, source: source, category: category_process, identifier: 'receive_vote' )
badge = Badge.create!(name: 'Iniciativas Ouro', description: 'Ganha ao receber 20 votos na sua proposta de iniciativa', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','badge_proposta_ouro.png'),'rb'))
Rule.create!(name: 'Receber Voto em Iniciativas Ouro', activity_type: activity_type, badge: badge , amount: 20)
badge = Badge.create!(name: 'Iniciativas Prata', description: 'Ganha ao receber 10 votos na proposta de iniciativa.', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','badge_proposta_prata.png'),'rb'))
Rule.create!(name: 'Receber Voto em Iniciativas Prata', activity_type: activity_type, badge: badge , amount: 10)
badge = Badge.create!(name: 'Iniciativas Bronze', description: 'Ganha ao receber 5 votos em suas propostas de iniciativa.', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','badge_proposta_bronze.png'),'rb'))
Rule.create!(name: 'Receber Voto em Iniciativas Bronze', activity_type: activity_type, badge: badge , amount: 1)


ActivityType.create!(name: 'Ter uma ideia aprovada pela diretoria', description: 'Ideia outro aprovada pela diretoria', points_definition: 500, source: source, category: category_process, identifier: 'approved_proposal' )


badge_group = BadgeGroup.create!(:name => 'Votar em Propostas do Planejamento Estratégico')
activity_type = ActivityType.create!(name: 'Votar numa proposta do Planejamento', description: 'Participacao na definicao de propostas vencedoras do planejamento', points_definition: 5, source: source, category: category_process, identifier: 'vote_proposal' )
badge = Badge.create!(name: 'Votar em Ideias Ouro', description: 'Ganha quando votar em 100 propostas de iniciativas.', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','badge_votos_ouro.png'),'rb'))
Rule.create!(name: 'Votar Iniciativas Ouro', activity_type: activity_type, badge: badge , amount: 100)
badge = Badge.create!(name: 'Votar em Ideias Prata', description: 'Ganha quando votar em 25 propostas de iniciativas.', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','badge_votos_prata.png'),'rb'))
Rule.create!(name: 'Votar Iniciativas Prata', activity_type: activity_type, badge: badge , amount: 25)
badge = Badge.create!(name: 'Votar Ideias Bronze', description: 'Ganha quando votar em 5 propostas de iniciativas.', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','badge_votos_bronze.png'),'rb'))
Rule.create!(name: 'Votar Iniciativas Bronze', activity_type: activity_type, badge: badge , amount: 5)

admin = User.create(:email => 'admin@localhost.com', :password => '12345678', :password_confirmation => '12345678', :name => 'admin')
admin.confirm
user = admin
# Activity.create!(title: "#{user.name} entrou na comunidade do planejamento estratégico", activity_type: activity_type, user: user)
# user = create_user
# Activity.create!(title: "#{user.name} entrou na comunidade do planejamento estratégico", activity_type: activity_type, user: user)


StrategicObjective.destroy_all
StrategicDriver.destroy_all
Proposal.destroy_all

objective = StrategicObjective.create!(title: 'OE2 – Faturamento anual', description: 'OE2 – Atingir o faturamento anual de R$ 3,08 bi em 2018')

driver = StrategicDriver.create!(:title => 'D2.1 – Atingir o faturamento anual de R$ 146 mi na linha de negócios Serviços de Informação em 2018', :objective => objective)

# 1.upto(4).map do
#   proposals_count += 1
#   Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver )
# end
driver = StrategicDriver.create!(:title => 'D2.2 – Atingir o faturamento anual de R$ 368 mi na linha de negócios Serviços em Nuvem em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D2.3 – Atingir o faturamento anual de  R$ 2.57 bi na linha de negócio Soluções sob Medida em 2018', :objective => objective)

objective = StrategicObjective.create!(title: 'OE3 - Satisfação dos clientes', description: 'OE3 - Atender proativamente as necessidades dos clientes para elevar o nível de satisfação geral a pelo menos 76% em 2018')
driver = StrategicDriver.create!(:title => 'D3.1 – Alcançar nível de satisfação geral com o atendimento ao cliente de 85% em 2018', :objective => objective)
#Proposal.create(:title => '', :description => '', :status => Proposal::ACTIVE, :driver => driver )
 proposals_count = 0
# 1.upto(4).map do
#   proposals_count += 1
#   Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver)
# end
driver = StrategicDriver.create!(:title => 'D3.2 - Alcançar nível de satisfação geral com os serviços do Serpro de 75% em 2018 ', :objective => objective)
 1.upto(20).map do
   proposals_count += 1
   Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver, user: admin )
 end
driver = StrategicDriver.create!(:title => 'D3.3 – Alcançar nível de satisfação com o prazo de entrega de serviços de 64% em 2018 ', :objective => objective)
 1.upto(40).map do
   proposals_count += 1
   Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver, user: admin )
 end
driver = StrategicDriver.create!(:title => 'D3.4 - Aumentar o nível de reconhecimento do atributo de imagem Inovação junto ao cliente para 60% em 2018', :objective => objective)


objective = StrategicObjective.create!(title: 'OE4 – Diversificação de carteira', description: 'OE4 – Diversificar a carteira de negócios para aumentar a representatividade de clientes não dependentes do OGU a 10% com no mínimo R$ 308 mi de faturamento em 2018')
driver = StrategicDriver.create!(:title => 'D4.1 – Representatividade de 1% (alcançando no mínimo R$ 30,8 mi de faturamento) no governo federal não OGU em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D4.2 – Representatividade de 2% (alcançando no mínimo R$ 61,7 mi de faturamento) no governo estadual e municipal em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D4.3 – Representatividade de 7% (alcançando no mínimo R$ 216 mi de faturamento) em empresas privadas em 2018', :objective => objective)
objective = StrategicObjective.create!(title: 'OE5 – Limite de despesas', description: 'OE5 – Limitar a despesa total ao montante de R$ 2,55 bi em 2018')
driver = StrategicDriver.create!(:title => 'D5.1 - Limitar o montante da folha de pagamento em R$ 1,9 bi em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D5.2 - Limitar as despesas com contratos de TI ao montante de R$ 291 mi em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D5.3 - Limitar as despesas com contratos administrativos ao montante de R$ 147,9 mi em 2018 ', :objective => objective)
objective = StrategicObjective.create!(title: 'OE6 – Soluções multicliente', description: 'Elevar em pelo menos 10% de efetividade o desenvolvimento de soluções multiclientes em 2018')
driver = StrategicDriver.create!(:title => 'D6.1 – Aprimorar a construção de soluções para entregar 80% das soluções de multiclientes em até 48 dias em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D6.2 – Atingir o faturamento anual de R$ 308 mi com produtos oriundos de parceria com exploração de dados do Governo Federal em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D6.3 – Atingir o faturamento anual de R$ 256 mi com novos produtos de soluções multiclientes lançados nos últimos 24 meses em 2018', :objective => objective)
objective = StrategicObjective.create!(title: 'OE7 – Soluções sob medida', description: 'OE7 - Elevar em pelo menos 15% o desempenho das entregas em soluções de software sob medida em 2018')
driver = StrategicDriver.create!(:title => 'D7.1 - Reduzir 15% do tempo de atendimento de demandas de soluções de software sob medida em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D7.2 - Aumentar em 15% a qualidade das soluções de software sob medida em 2018 ', :objective => objective)

objective = StrategicObjective.create!(title: 'OE8 – Cultura organizacional', description: 'OE8 – Desenvolver a cultura organizacional para atingir a média de 73,1% nas melhores práticas de gestão de pessoas até abril de 2019')
driver = StrategicDriver.create!(:title => 'D8.1 - Alcançar pelo menos 73% no nível de satisfação com relação ao aspecto em liderança', :objective => objective)
driver = StrategicDriver.create!(:title => 'D8.2 - Alcançar pelo menos  70% no nível de satisfação com relação ao aspecto em cultura de participação e autonomia', :objective => objective)
driver = StrategicDriver.create!(:title => 'D8.3 - Alcançar pelo menos 66% no nível de satisfação com relação ao aspecto em conhecimento e educação corporativa', :objective => objective)

objective = StrategicObjective.create!(title: 'OE9 – Competências estratégicas', description: 'OE9 – Elevar em pelo menos 15% a proficiência das competências estratégicas para garantir a sustentabilidade empresarial em 2018')
driver = StrategicDriver.create!(:title => 'D9.1 – Elevar 15% na proficiência nas competências gerais de Engajamento, Integridade, Proatividade e Excelência em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D9.2 – Elevar 15% na proficiência nas competências dos empregados de Responsividade, Inovação, Transversalidade e Foco no Cliente em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D9.3 – Elevar 15% na proficiência nas competências das lideranças de Gerenciamento de Divergências, Valorização das Pessoas, Disseminação de Informações e Atuação Estratégica em 2018', :objective => objective)

objective = StrategicObjective.create!(title: 'OE10 – Alavancar a inovação', description: 'OE10 - Alavancar a inovação para proporcionar pelo menos 4 soluções digitais inovadoras e 4 co-criações em 2018')
driver = StrategicDriver.create!(:title => 'D10.1 – Desenvolver e comercializar 2 soluções inovadoras baseadas em analytics em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D10.2 – Desenvolver e comercializar 2 soluções inovadoras baseadas em inteligência artificial em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D10.3 – 2 categorias de dispositivos homologadas, na modalidade de co-criação, para a plataforma de IoT em 2018', :objective => objective)
driver = StrategicDriver.create!(:title => 'D10.4 – 2 redes corporativas disponibilizadas, na modalidade de co-criação, baseada em Blockchain em 2018', :objective => objective)
