# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171030142634) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string   "title"
    t.text     "details"
    t.integer  "points_of_action"
    t.integer  "activity_type_id"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["activity_type_id"], name: "index_activities_on_activity_type_id", using: :btree
    t.index ["user_id"], name: "index_activities_on_user_id", using: :btree
  end

  create_table "activity_types", force: :cascade do |t|
    t.string   "name"
    t.string   "identifier"
    t.text     "description"
    t.integer  "points_definition"
    t.integer  "source_id"
    t.integer  "category_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["category_id"], name: "index_activity_types_on_category_id", using: :btree
    t.index ["source_id"], name: "index_activity_types_on_source_id", using: :btree
  end

  create_table "badge_groups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "badges", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "badge_group_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["badge_group_id"], name: "index_badges_on_badge_group_id", using: :btree
  end

  create_table "badges_users", id: false, force: :cascade do |t|
    t.integer "badge_id", null: false
    t.integer "user_id",  null: false
    t.index ["badge_id", "user_id"], name: "index_badges_users_on_badge_id_and_user_id", unique: true, using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "levels", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "start_range"
    t.integer  "end_range"
    t.integer  "position"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "proposals", force: :cascade do |t|
    t.string   "title"
    t.text     "authors"
    t.text     "contribution"
    t.text     "description"
    t.text     "feedback"
    t.integer  "status",              default: 0
    t.integer  "votes_count",         default: 0
    t.integer  "strategic_driver_id"
    t.integer  "user_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["strategic_driver_id"], name: "index_proposals_on_strategic_driver_id", using: :btree
    t.index ["user_id"], name: "index_proposals_on_user_id", using: :btree
  end

  create_table "rules", force: :cascade do |t|
    t.string   "name"
    t.integer  "amount"
    t.datetime "begin_period"
    t.datetime "end_period"
    t.integer  "activity_type_id"
    t.integer  "badge_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["activity_type_id"], name: "index_rules_on_activity_type_id", using: :btree
    t.index ["badge_id"], name: "index_rules_on_badge_id", using: :btree
  end

  create_table "sources", force: :cascade do |t|
    t.string   "name"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "strategic_drivers", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "strategic_objective_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["strategic_objective_id"], name: "index_strategic_drivers_on_strategic_objective_id", using: :btree
  end

  create_table "strategic_objectives", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "login"
    t.integer  "points",                 default: 0
    t.integer  "level_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.text     "tokens"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["level_id"], name: "index_users_on_level_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "proposal_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["proposal_id"], name: "index_votes_on_proposal_id", using: :btree
    t.index ["user_id", "proposal_id"], name: "index_votes_on_user_id_and_proposal_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_votes_on_user_id", using: :btree
  end

  add_foreign_key "votes", "proposals"
  add_foreign_key "votes", "users"
end
