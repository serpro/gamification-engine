class CreateActivityTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :activity_types do |t|
      t.string :name
      t.string :identifier
      t.text :description
      t.integer :points_definition
      t.belongs_to :source
      t.belongs_to :category

      t.timestamps
    end
  end
end
