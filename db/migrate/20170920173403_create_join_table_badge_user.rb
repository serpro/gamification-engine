class CreateJoinTableBadgeUser < ActiveRecord::Migration[5.0]
  def change
    create_join_table :badges, :users do |t|
      t.index [:badge_id, :user_id], unique: true
    end
  end
end
