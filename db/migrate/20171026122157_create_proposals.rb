class CreateProposals < ActiveRecord::Migration[5.0]
  def change
    create_table :proposals do |t|
      t.string :title
      t.text :authors
      t.text :contribution
      t.text :description
      t.text :feedback
      t.integer :status, default: Proposal::INACTIVE
      t.integer :votes_count, default: 0

      t.belongs_to :strategic_driver
      t.belongs_to :user

      t.timestamps
    end
  end
end
