class CreateBadges < ActiveRecord::Migration[5.0]
  def change
    create_table :badges do |t|
      t.string :name
      t.text :description
      t.integer :position
      t.attachment :picture
      t.belongs_to :badge_group

      t.timestamps
    end
  end
end
