class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :proposal, foreign_key: true

      t.timestamps
    end
    add_index(:votes, [:user_id, :proposal_id], unique: true)

  end
end
