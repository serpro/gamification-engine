class LevelSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :start_range, :end_range
end
