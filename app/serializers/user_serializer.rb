class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :login, :nickname, :email, :level, :points, :badge_ids

end
