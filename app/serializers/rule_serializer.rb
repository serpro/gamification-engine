class RuleSerializer < ActiveModel::Serializer
  attributes :id, :name, :amount, :begin_period, :end_period
end
