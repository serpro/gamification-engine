class StrategicObjective < ApplicationRecord
  has_many :strategic_drivers

  has_many :proposals,  through: :strategic_drivers

  alias_attribute :drivers, :strategic_drivers

end
