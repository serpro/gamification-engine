class Level < ApplicationRecord

  acts_as_list

  scope :last_level, -> { order('position DESC').first }
  scope :level_of_points, -> (points) { 
    points.nil? ? order(:position).first : order(:position).where("start_range <= ? AND end_range >= ?", points, points).last
  }

  validate :custom_start_range_validation
  validate :custom_end_range_validation

  def custom_start_range_validation
    last_level = self.class.last_level
    last_level = nil unless last_level.present?

    # Should the first level start_range be zero
    if(last_level.nil? && (self.start_range != 0))
      errors.add(:start_range, :invalid_value, message: "the first level start range must to be zero")
    end

    if(self.new_record? && (last_level.present?) && ((last_level.end_range + 1) != self.start_range))
      errors.add(:start_range, :invalid_value, message: "the level start range be the last level end range plus one")
    end
  end

  def custom_end_range_validation
    if(self.end_range <= self.start_range)
      errors.add(:end_range, :invalid_value, message: "the level end range must be greater than level start range")
    end
  end

end
