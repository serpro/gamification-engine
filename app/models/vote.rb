class Vote < ApplicationRecord
  belongs_to :user
  belongs_to :proposal, counter_cache: true
  validates :user_id, uniqueness: { scope: :proposal_id }

  scope :by_user, -> user {
    where(user: user)
  }

  after_create :generate_points
  
  def generate_points
    at = ActivityType.where(identifier: 'vote_proposal').first    
    return nil if at.nil?
    if(self.user != self.proposal.user)
      Activity.create!(title: "#{self.user.name} votou na proposta #{self.proposal.title}", activity_type: at, user: self.user) 
    end
    
    at = ActivityType.where(identifier: 'receive_vote').first    
    return nil if at.nil?
    if(self.user != self.proposal.user)
      Activity.create!(title: "#{self.proposal.user.name} recebeu voto na proposta  #{self.proposal.title} do usuario #{self.user.name}", activity_type: at, user: self.proposal.user) 
    end
    
  end

end
