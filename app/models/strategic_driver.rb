class StrategicDriver < ApplicationRecord
  belongs_to :strategic_objective
  alias_attribute :objective, :strategic_objective

  has_many :proposals

  def objective_id
    self.objective.id if self.objective.present?
  end

end
