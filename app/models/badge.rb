class Badge < ApplicationRecord
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: ''

  validates_attachment_content_type :picture, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  belongs_to :badge_group, required: false
  alias_attribute :group, :badge_group
  alias_attribute :group_id, :badge_group_id

  acts_as_list scope: :badge_group


end
