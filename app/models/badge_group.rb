class BadgeGroup < ApplicationRecord
  
  has_many :badges

  validates :name, presence: true
  validates_uniqueness_of :name

end
