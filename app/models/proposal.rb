class Proposal < ApplicationRecord

  INACTIVE = 0
  ACTIVE = 1
  REJECTED = 2

  belongs_to :strategic_driver
  belongs_to :user
  has_many :votes
  has_many :voting_users, through: :votes, source: :user
  alias_attribute :driver, :strategic_driver
  alias_attribute :driver_id, :strategic_driver_id

  validate :is_owner
  validates :title, presence: true
  validate :has_no_vote

  after_create :generate_points

 
  def generate_points
    at = ActivityType.where(identifier: 'make_proposal').first    
    return nil if at.nil?
    Activity.create!(title: "#{self.user.name} fez uma proposta no direcionar estrategico #{self.driver.title}", activity_type: at, user: self.user) 
  end

  def vote(current_user)
    vote = Vote.new(user: current_user, proposal: self)
    vote.save
  end

  def voting_users_ids
    self.voting_users.pluck(:id)
  end

  private

    def has_no_vote
     self.errors.add(:already_voted, "Sua proposta ja obteve votos e não pode ser modificada.") if self.votes.count != 0
    end

    def is_owner
     if user_id_changed? && self.persisted?
       errors.add(:user_id, "Change of user is not allowed!")
     end   
    end

end
