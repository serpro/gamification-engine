class StrategicDriversController < ApplicationController

  before_action :authenticate_user!

  before_action :set_strategic_objective
  before_action :set_strategic_driver, only: [:show, :update, :destroy]

  # GET /strategic_drivers
  def index
    @strategic_drivers = @strategic_objective.drivers

    render json: @strategic_drivers
  end

#  # GET /strategic_drivers/1
#  def show
#    render json: @strategic_driver
#  end
#
#  # POST /strategic_drivers
#  def create
#    @strategic_driver = StrategicDriver.new(strategic_driver_params)
#
#    if @strategic_driver.save
#      render json: @strategic_driver, status: :created, location: @strategic_driver
#    else
#      render json: @strategic_driver.errors, status: :unprocessable_entity
#    end
#  end
#
#  # PATCH/PUT /strategic_drivers/1
#  def update
#    if @strategic_driver.update(strategic_driver_params)
#      render json: @strategic_driver
#    else
#      render json: @strategic_driver.errors, status: :unprocessable_entity
#    end
#  end
#
#  # DELETE /strategic_drivers/1
#  def destroy
#    @strategic_driver.destroy
#  end

  private
    def set_strategic_objective
      @strategic_objective = StrategicObjective.find(params[:strategic_objective_id])
      raise 'strategic_objective could not be loaded' unless @strategic_objective
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_strategic_driver
      @strategic_driver = @strategic_objective.drivers.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def strategic_driver_params
      params.require(:strategic_driver).permit(:title, :description, :strategic_objective_id)
    end
end
