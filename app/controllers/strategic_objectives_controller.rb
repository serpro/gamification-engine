class StrategicObjectivesController < ApplicationController

  before_action :authenticate_user!
  before_action :set_strategic_objective, only: [:show, :update, :destroy]

  # GET /strategic_objectives
  def index
    @strategic_objectives = StrategicObjective.all

    render json: @strategic_objectives
  end

  # GET /strategic_objectives/1
  def show
    render json: @strategic_objective
  end

#  # POST /strategic_objectives
#  def create
#    @strategic_objective = StrategicObjective.new(strategic_objective_params)
#
#    if @strategic_objective.save
#      render json: @strategic_objective, status: :created, location: @strategic_objective
#    else
#      render json: @strategic_objective.errors, status: :unprocessable_entity
#    end
#  end
#
#  # PATCH/PUT /strategic_objectives/1
#  def update
#    if @strategic_objective.update(strategic_objective_params)
#      render json: @strategic_objective
#    else
#      render json: @strategic_objective.errors, status: :unprocessable_entity
#    end
#  end
#
#  # DELETE /strategic_objectives/1
#  def destroy
#    @strategic_objective.destroy
#  end
#
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_strategic_objective
      @strategic_objective = StrategicObjective.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def strategic_objective_params
      params.require(:strategic_objective).permit(:title, :description)
    end
end
