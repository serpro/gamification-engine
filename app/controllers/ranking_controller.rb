class RankingController < ApplicationController

  def users
    page = params[:page] || 1
    per_page = params[:per_page] || 10

    @users = User.all.order(points: :desc).page(page).per(per_page)

    render json: @users
  end 

  def proposals
    page = params[:page] || 1
    per_page = params[:per_page] || 10

    @proposals = Proposal.all.order(votes_count: :desc).page(page).per(per_page)

    render json: @proposals
  end 

end
