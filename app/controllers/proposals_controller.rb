class ProposalsController < ApplicationController

  before_action :authenticate_user!

  before_action :set_strategic_objective
  before_action :set_proposal, only: [:show, :update, :vote]


  # GET /proposals
  def index
    page = params[:page] || 1
    per_page = params[:per_page] || 10
    @proposals = @strategic_objective.proposals.order(created_at: :desc).page(page).per(per_page)

    render json: @proposals
  end

  # GET /proposals/1
  def show
    render json: @proposal
  end

  def vote
    if @proposal.vote(current_user)
      render json: @proposal, status: :created
    else
      response = {}
      response[:code] = :duplicate_vote
      response[:message] = 'could not vote in the same proposal'
      render json: response, status: :unprocessable_entity
    end
  end


  # POST /proposals
  def create
    strategic_driver = @strategic_objective.strategic_drivers.find(params[:driver_id])
    @proposal = strategic_driver.proposals.build(proposal_params)
    @proposal.user = current_user

    if @proposal.save
      render json: @proposal, status: :created
    else
      render json: @proposal.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /proposals/1
  def update
    if @proposal.update(proposal_params)
      render json: @proposal
    else
      render json: @proposal.errors, status: :unprocessable_entity
    end
  end

#  # DELETE /proposals/1
#  def destroy
#    @proposal.destroy
#  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_proposal
      @proposal = @strategic_objective.proposals.find(params[:id])
    end

    def set_strategic_objective
      @strategic_objective = StrategicObjective.find(params[:strategic_objective_id])
      raise "strategic objective could not be null" unless @strategic_objective
      @strategic_objective
    end

    # Only allow a trusted parameter "white list" through.
    def proposal_params
      params.require(:proposal).permit(:title, :description, :strategic_objective_id, :driver_id, :contribution, :authors)
    end
end
