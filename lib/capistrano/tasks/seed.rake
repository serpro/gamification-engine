desc "Populate database"
task :seed do
  on roles(:all) do
    within fetch(:site_dir) do
      execute :rake, 'RAILS_ENV=production db:seed'
    end
  end
end
