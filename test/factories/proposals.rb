FactoryGirl.define do
  factory :proposal do
    title "MyString"
    description "MyText"
    feedback "MyText"
    status 1
  end
end
