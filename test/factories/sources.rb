FactoryGirl.define do

  factory :source do
    name { Faker::Name.unique.name }
    token "MyString"
  end

end
