FactoryGirl.define do

  factory :badge do
    name { Faker::Name.unique.name }
  end

end
