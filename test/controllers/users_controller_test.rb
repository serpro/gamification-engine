require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  setup do
    @user = create(:user)
  end
    

  USER_FIELDS_API.map do |field|
    define_method "test_should_display_field_#{field}_in_user_json" do
      get users_url, as: :json
      json = JSON.parse(response.body)
      assert json.first.has_key?(field)
    end 
  end

#  test "should get index" do
#    get users_url, as: :json
#    assert_response :success
#  end
#
#  test "should create user" do
#    assert_difference('User.count') do
#      post users_url, params: { user: { login: @user.login } }, as: :json
#    end
#
#    assert_response 201
#  end
#
#  test "should show user" do
#    get user_url(@user), as: :json
#    assert_response :success
#  end
#
#  test "should update user" do
#    patch user_url(@user), params: { user: { login: @user.login } }, as: :json
#    assert_response 200
#  end
#
#  test "should destroy user" do
#    assert_difference('User.count', -1) do
#      delete user_url(@user), as: :json
#    end
#
#    assert_response 204
#  end
end
