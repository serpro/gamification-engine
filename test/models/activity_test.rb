require 'test_helper'

class ActivityTest < ActiveSupport::TestCase

  test "reject empty name" do
    item = Activity.new
    !item.valid?
    assert item.errors[:title].present?
    item.title = 'Some'
    !item.valid?
    assert !item.errors[:title].present?
  end

  test "activity type is not null" do
    activity = Activity.new
    activity.valid?
    assert activity.errors.details[:activity_type].present?
  end

  test "user is not null" do
    activity = Activity.new
    activity.valid?
    assert activity.errors.details[:user].present?
  end

  test "update points_of_action according action type point definition" do
    activity_type = create(:activity_type, points_definition: 38)

    activity = Activity.new(activity_type: activity_type)
    activity.valid?
    assert_equal activity_type.points_definition, activity.points_of_action
  end

  test "points_definition be equal to activity_type points_definition" do
    activity_type = create(:activity_type, points_definition: 38)

    activity = Activity.new(activity_type: activity_type)
    activity.valid?
    assert_equal activity_type.points_definition, activity.points_definition
  end

  test "points_definition be nil if there is no activity_type" do
    activity = Activity.new()
    assert_nil activity.points_definition
  end

  test "by_user return the activities filter by user" do
    user1 = create(:user)
    user2 = create(:user)
    a1 = create(:activity, user: user1)
    create(:activity, user: user2)
    a2 = create(:activity, user: user1)
    assert_equal [a1,a2], Activity.by_user(user1)
  end

  test "by_user return empty array if no user is passed as parameters" do
    create(:activity)
    create(:activity)
    assert_equal [], Activity.by_user(nil)
  end

end
