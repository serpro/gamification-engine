require 'test_helper'

class BadgeTest < ActiveSupport::TestCase

   test "badge set position on creation" do
     badge = create(:badge)
     assert_equal 1, badge.position
     badge.position = nil
     badge.reload
     assert_equal 1, badge.position
   end

   test "badge position be defined in order of creation" do
     badge = create(:badge)
     assert_equal 1, badge.position
     badge = create(:badge)
     assert_equal 2, badge.position
   end

   test "badge be associated to badge_group or group" do
     badge_group = create(:badge_group)

     badge = create(:badge, badge_group: badge_group)
     assert_equal badge_group, badge.badge_group

     badge = create(:badge, group: badge_group)
     assert_equal badge_group, badge.group
   end

end
