require 'test_helper'

class ActivityTypeTest < ActiveSupport::TestCase

  test "reject empty name" do
    item = ActivityType.new
    !item.valid?
    assert item.errors[:name].present?
    item.name = 'Some'
    !item.valid?
    assert !item.errors[:name].present?
  end

  test "should the name be unique in source scope" do
    name = 'unique name'
    some_activity_type = create(:activity_type, name: name )
    activity_type = ActivityType.new
    activity_type.name = name
    activity_type.source = some_activity_type.source
    activity_type.valid?
    assert activity_type.errors.include?(:name)
  end

  test "should the name be unique only in source scope" do
    name = 'unique name'
    some_activity_type = create(:activity_type, name: name )
    activity_type = ActivityType.new
    activity_type.name = name
    activity_type.source = some_activity_type.source
    activity_type.valid?
    assert activity_type.errors.include?(:name)
    activity_type.source = create(:source)
    activity_type.valid?
    refute activity_type.errors.include?(:name)
  end

end
