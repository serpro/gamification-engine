Rails.application.routes.draw do

  scope :api do
#    resources :rules
    resources :ranking do 
      collection do
        get :users
        get :proposals
      end
    end

    resources :strategic_objectives do
      resources :strategic_drivers
      resources :proposals do
        member do
          post :vote
        end
      end
    end
    resources :users
    resources :levels
    resources :sources
    resources :action_types
    resources :activities
    resources :badges do
      collection do
        get :my
      end
    end
    resources :categories

    mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      sessions:  'overrides/sessions'
    }
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
